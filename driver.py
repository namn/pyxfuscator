from __future__ import absolute_import
from __future__ import with_statement

import py_compile
import types
from util import code

from block import Block
import code2graph
import decompyle

def code_to_graph():
    import marshal
    file = open(r"code2graph.pyc", "rb")
    file.seek(8)
    co = marshal.loads(file.read())
    file.close()
    for c in co.co_consts:
        if type(c) == types.CodeType:
            code2graph.code2dot(c, open("code2graph.dot", "w"))
            break
    blocks = Block.get_blocks(co)
    for block in blocks:
        print block

def decompile():
    py_compile.compile("test_source.py", "test_source.pyc")
    co = code.get_code_object("test_source.pyc")
    
    from pyxfuscator import PrettyPrinter
    from compiler import visitor, transformer
    
    ast = transformer.parseFile("test_source.py")
    print ast
    
    decompiler = decompyle.Decompiler25(co)
    ast = decompiler.decompile()
    
    walker = visitor.ASTVisitor()
    v = PrettyPrinter()
    walker.preorder(ast, v)

code_to_graph()
