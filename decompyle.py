from __future__ import absolute_import
from __future__ import with_statement

from opcode import opname
from types import CodeType, NoneType, ClassType

from typecheck import accepts, returns, Self, Class, IsOneOf

from block import Block, Label, Instruction
from compiler import ast
from copy import deepcopy

import re
import sys

NONE = ast.Const(None)

RETURN_NONE = ast.Return(NONE)

def compare_node(a, b):
    return cmp(repr(a), repr(b))


class HintEnum(int):
    pass


HintEnum.HINT_STACK = HintEnum(0)
HintEnum.HINT_STATEMENT = HintEnum(1)
HintEnum.HINT_PASS = HintEnum(2)
HintEnum.HINT_FRAGMENT = HintEnum(3)
HintEnum.HINT_AND = HintEnum(4)
HintEnum.HINT_OR = HintEnum(5)


DecompileHint = Class("DecompileHint")

class DecompileHint(object):
    
    @accepts(Self(), int, int, IsOneOf(ast.Node, NoneType), HintEnum)
    def __init__(self, begin, end, node, t=HintEnum.HINT_STACK):
        self.begin = begin
        self.end = end
        self.node = node
        self.hint_type = t
        self.hints = []
    
    @accepts(Self(), DecompileHint)
    @returns(Self())
    def add_hint(self, hint):
        self.hints.append(hint)
        return self
    
    @accepts(Self(), (int, int))
    @returns(Self())
    def add_fragments(self, ranges):
        for beg, end in ranges:
            self.hints.append(DecompileHint(beg, end, None, t=HintEnum.HINT_FRAGMENT))
        return self

    @staticmethod
    @accepts([DecompileHint])
    @returns([DecompileHint])
    def fill_pass(hints):
        hints = hints[:]
        min_ = min((h.begin for h in hints))
        max_ = max((h.end for h in hints))
        bitset = [0] * (max_ - min_ + 1)
        for h in hints:
            for i in xrange(h.begin - min_, h.end - min_):
                bitset[i] = 1
        i = 0
        while i < len(bitset):
            i = i + 1
            if not bitset[i]:
                start = i + min_
                while i < len(bitset) and not bitset[i]:
                    i = i + 1
                stop = i - 1 + min_
                hints.append(DecompileHint(start, stop, None, HintEnum.HINT_PASS))
        return hints
        
class Decompiler(object):
    
    @accepts(Self(), CodeType, IsOneOf(list, NoneType), int, int)
    def __init__(self, code, hints=None, begin=0, end=sys.maxint):
        self.stack = []
        self.code = code
        self.blocks = Block.get_blocks(code)
        self.current_position = begin
        self.stmts = []
        self.assign_params = [None, None]
        self.assign_stack = []
        if hints is None:
            self.hints = []
        else:
            self.hints = hints
        self.end = end
    
    @accepts(Self(), int)
    @returns(IsOneOf(Instruction, NoneType))
    def get_instruction(self, position):
        for block in self.blocks:
            for inst in block.insts:
                if inst.position > self.end:
                    return None
                if inst.position >= position:
                    return inst
        return None

    @accepts(Self())
    @returns(IsOneOf(Instruction, NoneType))
    def get_next_instruction(self):
        inst = self.get_instruction(self.current_position)
        if inst:
            self.current_position = inst.position + 1
        return inst
    
    @accepts(Self(), Instruction)
    @returns(NoneType)
    def visit(self, inst):
        name = opname[inst.opcode]
        func_name = "visit_" + name.lower().replace('+', '_')
        try:
            func = getattr(self, func_name)
        except AttributeError:
            print "missing", func_name
            return
        func(inst)

    @accepts(Self())
    @returns(ast.Node)
    def decompile(self):
        inst = self.get_next_instruction()
        while inst:
            for hint in self.hints:
                if (inst.position >= hint.begin) and (inst.position <= hint.end):
                    if hint.hint_type == HintEnum.HINT_STACK:
                        self.stack.append(hint.node)
                    elif hint.hint_type == HintEnum.HINT_STATEMENT:
                        self.stmts.append(hint.node)
                    elif hint.hint_type == HintEnum.HINT_FRAGMENT:
                        self.__decompile_fragment(hint.begin, hint.end)
                    elif hint.hint_type == HintEnum.HINT_AND:
                        self.__decompile_and(hint.begin, hint.end, hint)
                    elif hint.hint_type == HintEnum.HINT_OR:
                        self.__decompile_or(hint.begin, hint.end, hint)
                    self.current_position = hint.end + 1
                    break
            else:
                self.visit(inst)
            inst = self.get_next_instruction()
        if self.stmts:
            if compare_node(self.stmts[-1], RETURN_NONE) == 0:
                self.stmts.pop()
        else:
            return self.stack[-1]
        return ast.Stmt(self.stmts)
    
    def __decompile_fragment(self, begin, end, hint=None):
        '''Decompiles a fragment.'''
        d = self.__class__(self.code, None, begin, end)
        ret = d.decompile()
        if isinstance(ret, ast.Stmt):
            self.stmts.append(ret)
        else:
            self.stack.append(ret)
        return ret
    
    def __decompile_logic(self, begin, end, hint, class_):
        hints = DecompileHint.fill_pass(hint.hints)
        d = self.__class__(self.code, hints, begin, end)
        d.decompile()
        ret = class_(d.stack)
        self.stack.append(ret)
        return ret

    @returns(ast.And)
    def __decompile_and(self, begin, end, hint):
        return self.__decompile_logic(begin, end, hint, ast.And)
    
    @returns(ast.Or)
    def __decompile_or(self, begin, end, hint):
        return self.__decompile_logic(begin, end, hint, ast.Or)

    
class Decompiler25(Decompiler):

    @accepts(Self(), Instruction)
    def visit_load_name(self, inst):
        self.stack.append(ast.Name(inst.oparg))
    
    visit_load_fast = visit_load_name
    visit_load_global = visit_load_name

    #@accepts(Self(), Instruction)
    #def visit_load_const(self, inst):
    #    self.stack.append(ast.Const(inst.oparg))

    @accepts(Self(), Instruction, ClassType)
    def visit_binary_operator(self, inst, klass):
        right = self.stack.pop()
        left = self.stack.pop()
        self.stack.append(klass((left, right)))
    
    @accepts(Self(), Instruction)
    def visit_binary_lshift(self, inst):
        self.visit_binary_operator(inst, ast.LeftShift)
    
    @accepts(Self(), Instruction)
    def visit_binary_rshift(self, inst):
        self.visit_binary_operator(inst, ast.RightShift)
    
    @accepts(Self(), Instruction)
    def visit_binary_and(self, inst):
        self.visit_binary_operator(inst, ast.Bitand)
    
    @accepts(Self(), Instruction)
    def visit_binary_or(self, inst):
        self.visit_binary_operator(inst, ast.Bitor)
    
    @accepts(Self(), Instruction)
    def visit_binary_xor(self, inst):
        self.visit_binary_operator(inst, ast.Bitxor)
    
    @accepts(Self(), Instruction)
    def visit_binary_add(self, inst):
        self.visit_binary_operator(inst, ast.Add)
    
    @accepts(Self(), Instruction)
    def visit_binary_subtract(self, inst):
        self.visit_binary_operator(inst, ast.Sub)
    
    @accepts(Self(), Instruction)
    def visit_binary_multiply(self, inst):
        self.visit_binary_operator(inst, ast.Mul)

    @accepts(Self(), Instruction)
    def visit_binary_divide(self, inst):
        self.visit_binary_operator(inst, ast.Div)
    
    @accepts(Self(), Instruction)
    def visit_binary_floor_divide(self, inst):
        self.visit_binary_operator(inst, ast.FloorDiv)
    
    @accepts(Self(), Instruction)
    def visit_binary_modulo(self, inst):
        self.visit_binary_operator(inst, ast.Mod)
    
    @accepts(Self(), Instruction)
    def visit_binary_power(self, inst):
        self.visit_binary_operator(inst, ast.Power)
    
    @accepts(Self(), Instruction, str)
    def visit_inplace_operator(self, inst, operator):
        value = self.stack.pop()
        value.inplace = operator
        expr = self.stack.pop() # ignored
        self.stack.append(value)

    @accepts(Self(), Instruction)
    def visit_inplace_add(self, inst):
        self.visit_inplace_operator(inst, '+=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_subtract(self, inst):
        self.visit_inplace_operator(inst, '-=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_multiply(self, inst):
        self.visit_inplace_operator(inst, '*=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_divide(self, inst):
        self.visit_inplace_operator(inst, '/=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_floor_divide(self, inst):
        self.visit_inplace_operator(inst, '//=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_power(self, inst):
        self.visit_inplace_operator(inst, '**=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_lshift(self, inst):
        self.visit_inplace_operator(inst, '<<=')
    
    @accepts(Self(), Instruction)
    def visit_inplace_rshift(self, inst):
        self.visit_inplace_operator(inst, '>>=')
    
    @accepts(Self(), Instruction)
    @returns([ast.Node])
    def get_arguments(self, inst):
        param_count = inst.oparg
        positional = param_count & 0xFF
        keyword = param_count >> 8
        args = []
        while keyword > 0:
            value = self.stack.pop()
            key = self.stack.pop()
            args.append(ast.Keyword(key.value, value))
            keyword -= 1
        while positional > 0:
            args.append(self.stack.pop())
            positional -= 1
        args.reverse()
        return args
    
    @accepts(Self(), Instruction)
    def visit_call_function(self, inst):
        args = self.get_arguments(inst)
        node = self.stack.pop()
        self.stack.append(ast.CallFunc(node, args))
    
    @accepts(Self(), Instruction)
    def visit_call_function_var(self, inst):
        star_arg = self.stack.pop()
        args = self.get_arguments(inst)
        node = self.stack.pop()
        self.stack.append(ast.CallFunc(node, args, star_arg))

    @accepts(Self(), Instruction)
    def visit_call_function_kw(self, inst):
        dstar_arg = self.stack.pop()
        args = self.get_arguments(inst)
        node = self.stack.pop()
        self.stack.append(ast.CallFunc(node, args, None, dstar_arg))
    
    @accepts(Self(), Instruction)
    def visit_call_function_var_kw(self, inst):
        dstar_arg = self.stack.pop()
        star_arg = self.stack.pop()
        args = self.get_arguments(inst)
        node = self.stack.pop()
        self.stack.append(ast.CallFunc(node, args, star_arg, dstar_arg))
    
    @accepts(Self(), Instruction)
    def visit_print_item(self, inst):
        self.stmts.append(ast.Print([self.stack.pop()], None))

    @accepts(Self(), Instruction)
    def visit_print_newline(self, inst):
        last_stmt = self.stmts[-1]
        assert isinstance(last_stmt, ast.Print)
        last_stmt.__class__ = ast.Printnl
    
    @accepts(Self(), Instruction)
    def visit_dup_top(self, inst):
        self.stack.append(self.stack[-1])
    
    @accepts(Self(), Instruction)
    def visit_dup_topx(self, inst):
        count = inst.oparg
        self.stack.extend(self.stack[-count : ])
    
    @accepts(Self(), Instruction)
    def visit_rot_two(self, inst):
        '''1, 2 --> 2, 1'''
        first = self.stack.pop()
        second = self.stack.pop()
        rotated = [2, None]
        if 'rotated' not in dir(first):
            first.rotated = rotated
            second.rotated = rotated
        self.stack.append(first)
        self.stack.append(second)
    
    @accepts(Self(), Instruction)
    def visit_rot_three(self, inst):
        '''1, 2, 3 --> 2, 3, 1'''
        first = self.stack.pop()
        second = self.stack.pop()
        third = self.stack.pop()
        rotated = [3, None]
        if 'rotated' not in dir(first):
            first.rotated = rotated
            second.rotated = rotated
            third.rotated = rotated
        self.stack.append(first)
        self.stack.append(third)
        self.stack.append(second)
        
    @accepts(Self(), [str])
    def ignore_ahead(self, insts):
        old_position = self.current_position
        for next in insts:
            p = re.compile(next)
            inst = self.get_next_instruction()
            if not p.match(opname[inst.opcode]):
                self.current_position = old_position
                break
        
    @accepts(Self(), Instruction)
    def visit_print_item_to(self, inst):
        destination = self.stack.pop()
        node = self.stack.pop()
        self.stmts.append(ast.Print([node], destination))
        self.ignore_ahead(['POP_TOP'])

    @accepts(Self(), Instruction)
    def visit_print_newline_to(self, inst):
        last_stmt = self.stmts[-1]
        assert isinstance(last_stmt, ast.Print)
        last_stmt.__class__ = ast.Printnl

    @accepts(Self(), Instruction)
    def visit_exec_stmt(self, inst):
        globals = self.stack.pop()
        if compare_node(globals, NONE) == 0:
            globals = None
        locals = self.stack.pop()
        if compare_node(locals, NONE) == 0:
            locals = None
        if globals == locals:
            globals = None
        node = self.stack.pop()
        self.stmts.append(ast.Exec(node, locals, globals))
    
    @accepts(Self(), Instruction)
    def visit_import_name(self, inst):
        names = self.stack.pop()
        level = self.stack.pop()
        mod_name = inst.oparg
        if compare_node(names, NONE) == 0:
            self.stmts.append(ast.Import([(mod_name, None)]))
            self.ignore_ahead(['STORE_NAME'])
        else:
            self.stmts.append(ast.From(mod_name,
                    [(x.value, None) for x in names.nodes], level.value))

    @accepts(Self(), Instruction)
    def visit_import_star(self, inst):
        # handled by import_name
        pass
    
    @accepts(Self(), Instruction)
    def visit_import_from(self, inst):
        # handled by import_name
        # extra instructions to pop off the stack
        self.ignore_ahead(['STORE_NAME'])
        self.ignore_ahead(['POP_TOP'])
    
    @accepts(Self(), Instruction)
    def visit_pop_top(self, inst):
        node = self.stack.pop()
        self.stmts.append(ast.Discard(node))
        
    @accepts(Self(), Instruction)
    def visit_load_attr(self, inst):
        expr = self.stack.pop()
        name = inst.oparg
        self.stack.append(ast.Getattr(expr, name))
    
    def create_assign(self, value, ass_obj):
        
        def append_assign_param(lhs, rhs):
            if self.assign_params[0] is None:
                self.assign_params[0] = [lhs]
                self.assign_params[1] = rhs
            else:
                if isinstance(self.assign_params[0], list):
                    self.assign_params[0].append(lhs)
                    try:
                        if self.assign_params[1] != rhs:
                            self.assign_params[1].nodes.append(rhs)
                        else:
                            self.assign_params[1] = ast.Tuple([self.assign_params[1], rhs])
                    except AttributeError:
                        self.assign_params[1] = ast.Tuple([self.assign_params[1], rhs])
                elif isinstance(self.assign_params[0], ast.AssTuple):
                    self.assign_params[0].nodes.append(lhs)
                    if not isinstance(self.assign_params[1], ast.Tuple):
                        self.assign_params[1] = ast.Tuple([self.assign_params[1]])
                    self.assign_params[1].nodes.append(rhs)
                else:
                    alhs = self.assign_params[0]
                    arhs = self.assign_params[1]
                    if rhs != arhs:
                        self.assign_params[1] = ast.Tuple([arhs, rhs])
                        self.assign_params[0] = ast.AssTuple([alhs, lhs])
                    else:
                        self.assign_params[0] = [alhs, lhs]
        if 'inplace' in dir(value):
            assert len(self.stack) == 0
            self.stmts.append(ast.AugAssign(ass_obj, value.inplace, value))
            return

        if 'unpacked' in dir(value):
            unpacked = value.unpacked
            unpacked[0] -= 1
            if unpacked[1] is None:
                unpacked[1] = [ast.AssTuple([]), None]
            unpacked[1][0].nodes.append(ass_obj)
            if unpacked[0] == 0:
                unpacked[1][1] = value
                append_assign_param(*unpacked[1])
                if 'rotated' in dir(value):
                    value.rotated[0] -= 1
                    value.rotated[1] = [self.assign_params[0]]
            else:
                self.stack.append(value)
        elif 'rotated' in dir(value):
            append_assign_param(ass_obj, value)
            value.rotated[0] -= 1
            #if value.rotated[0] == 0:
                #value.rotated[1] = ast.AssTuple(
        else:
            append_assign_param(ass_obj, value)
        self.finish_assign()
        
    def finish_assign(self):
        if len(self.stack) == 0:
            if len(self.assign_params[0]) > 1:
                vfirst = None
                if len(self.assign_params[1].nodes) > 1:
                    vfirst = self.assign_params[1].nodes[0]
                    for v in self.assign_params[1].nodes:
                        if vfirst != v:
                            vfirst = None
                            break
                if vfirst:
                    self.assign_params[1] = vfirst
                else:
                    self.assign_params[0] = [ast.AssTuple(self.assign_params[0])]
            node = ast.Assign(self.assign_params[0], self.assign_params[1])
            self.stmts.append(node)
            self.assign_params = [None, None]
    
    @accepts(Self(), Instruction)
    def visit_store_attr(self, inst):
        name = inst.oparg
        expr = self.stack.pop()
        value = self.stack.pop()
        assattr = ast.AssAttr(expr, name, 'OP_ASSIGN')
        self.create_assign(value, assattr)
        
    @accepts(Self(), Instruction)
    def visit_store_name(self, inst):
        assname = ast.AssName(inst.oparg, 'OP_ASSIGN')
        value = self.stack.pop()
        self.create_assign(value, assname)

    visit_store_fast = visit_store_name
    visit_store_global = visit_store_name

    @accepts(Self(), Instruction)
    def visit_make_function(self, inst):
        import dis
        code = self.stack.pop().value
        d = self.__class__(code)
        node = d.decompile()
        #print '\n\n\n\n', node, '\n\n\n\n'
        if not node.nodes:
            node.nodes.append(ast.Pass())
        arg_count = code.co_argcount
        # have var arg?
        if code.co_flags & 0x04:
            arg_count += 1
        # have kw arg?
        if code.co_flags & 0x08:
            arg_count += 1
        default_args = [self.stack.pop() for x in range(inst.oparg)]
        default_args.reverse()
        func = ast.Function(None, code.co_name,
                            list(code.co_varnames[ : arg_count]),
                            default_args,
                            code.co_flags, None, node)
        self.stmts.append(func)
        self.ignore_ahead(['STORE_(NAME|FAST)'])
    
    @returns(ast.Node)
    def make_const(self, const):
        if isinstance(const, tuple):
            return ast.Tuple([self.make_const(x) for x in const])
        else:
            return ast.Const(const)
            
    @accepts(Self(), Instruction)
    def visit_load_const(self, inst):
        const = self.make_const(inst.oparg)
        self.stack.append(const)

    def build_list(self, inst, klass):
        ret = []
        count = inst.oparg
        while count > 0:
            count -= 1
            ret.append(self.stack.pop())
        ret.reverse()
        self.stack.append(klass(ret))
        
    @accepts(Self(), Instruction)
    def visit_build_list(self, inst):
        self.build_list(inst, ast.List)
    
    @accepts(Self(), Instruction)
    def visit_build_tuple(self, inst):
        self.build_list(inst, ast.Tuple)
    
    def visit_unpack_sequence(self, inst):
        sequence = self.stack.pop()
        #if 'nodes' in dir(sequence):
            #sequence = sequence.nodes
            #sequence.reverse()
            #rotated = [inst.oparg, None]
            #for n in sequence:
                #if 'rotated' not in dir(n):
                    #n.rotated = rotated
        #else:
            #sequence = 
        sequence.unpacked = [inst.oparg, None]
        self.stack.append(sequence)
            
    @accepts(Self(), Instruction)
    def visit_build_map(self, inst):
        count = inst.oparg
        ret = []
        while count > 0:
            count -= 1
            value = self.stack.pop()
            key = self.stack.pop()
            ret.append((key, value))
        self.stack.append(ast.Dict(ret))
    
    @accepts(Self(), Instruction)
    def visit_binary_subscr(self, inst):
        subscr = self.stack.pop()
        expr = self.stack.pop()
        self.stack.append(ast.Subscript(expr, 'OP_APPLY', [subscr]))
    
    @accepts(Self(), Instruction)
    def visit_store_subscr(self, inst):
        subscr = self.stack.pop()
        expr = self.stack.pop()
        value = self.stack.pop()
        if isinstance(expr, ast.Dict):
            # if expr is a dict, this is a
            # dict-building construct such as {1: 2}
            expr.items.append((subscr, value))
        else:    
            ass_subscr = ast.Subscript(expr, 'OP_ASSIGN', [subscr])
            self.create_assign(value, ass_subscr)

    @accepts(Self(), Instruction)
    def visit_unary_positive(self, inst):
        value = self.stack.pop()
        self.stack.append(ast.UnaryAdd(value))
    
    @accepts(Self(), Instruction)
    def visit_unary_negative(self, inst):
        value = self.stack.pop()
        self.stack.append(ast.UnarySub(value))
        
    @accepts(Self(), Instruction)
    def visit_compare_op(self, inst):
        right = self.stack.pop()
        left = self.stack.pop()
        self.stack.append(ast.Compare(left, [(inst.oparg, right)]))
    
    @accepts(Self(), Instruction)
    def visit_slice_0(self, inst):
        expr = self.stack.pop()
        self.stack.append(ast.Slice(expr, 'OP_APPLY', None, None))

    @accepts(Self(), Instruction)
    def visit_slice_1(self, inst):
        start = self.stack.pop()
        expr = self.stack.pop()
        self.stack.append(ast.Slice(expr, 'OP_APPLY', start, None))
    
    @accepts(Self(), Instruction)
    def visit_slice_2(self, inst):
        stop = self.stack.pop()
        expr = self.stack.pop()
        self.stack.append(ast.Slice(expr, 'OP_APPLY', None, stop))
    
    @accepts(Self(), Instruction)
    def visit_slice_3(self, inst):
        stop = self.stack.pop()
        start = self.stack.pop()
        expr = self.stack.pop()
        self.stack.append(ast.Slice(expr, 'OP_APPLY', start, stop))
    
    @accepts(Self(), Instruction)
    def visit_store_slice_0(self, inst):
        expr = self.stack.pop()
        slice = ast.Slice(expr, 'OP_ASSIGN', None, None)
        value = self.stack.pop()
        self.stmts.append(ast.Assign([slice], value))
    
    @accepts(Self(), Instruction)
    def visit_store_slice_1(self, inst):
        start = self.stack.pop()
        expr = self.stack.pop()
        slice = ast.Slice(expr, 'OP_ASSIGN', start, None)
        value = self.stack.pop()
        self.stmts.append(ast.Assign([slice], value))
    
    @accepts(Self(), Instruction)
    def visit_store_slice_2(self, inst):
        stop = self.stack.pop()
        expr = self.stack.pop()
        slice = ast.Slice(expr, 'OP_ASSIGN', None, stop)
        value = self.stack.pop()
        self.stmts.append(ast.Assign([slice], value))
    
    @accepts(Self(), Instruction)
    def visit_store_slice_3(self, inst):
        stop = self.stack.pop()
        start = self.stack.pop()
        expr = self.stack.pop()
        slice = ast.Slice(expr, 'OP_ASSIGN', start, stop)
        value = self.stack.pop()
        self.stmts.append(ast.Assign([slice], value))
    
    @accepts(Self(), Instruction)
    def visit_build_slice(self, inst):
        params = []
        param_count = inst.oparg
        while param_count > 0:
            param_count -= 1
            params.append(self.stack.pop())
        params.reverse()
        self.stack.append(ast.Sliceobj(params))
        
    @accepts(Self(), Instruction)
    def visit_return_value(self, inst):
        node = ast.Return(self.stack.pop())
        self.stmts.append(node)

    @accepts(Self(), Instruction)
    def visit_jump_if_false(self, inst):
        # TODO
        pass
    
    @accepts(Self(), Instruction)
    def visit_jump_if_true(self, inst):
        # TODO
        pass
