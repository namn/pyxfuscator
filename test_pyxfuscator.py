from compiler import transformer, visitor
import unittest
import StringIO
import sys

import pyxfuscator

class TestPrinter(unittest.TestCase):
    
    def setUp(self):
        self.buffer = StringIO.StringIO()
        self.visitor = pyxfuscator.PrettyPrinter(self.buffer)
        self.walker = visitor.ASTVisitor()
        
    def tearDown(self):
        self.buffer.close()
        self.buffer = None
    
    def compileAndCheck(self, source, expected):
        tree = transformer.parse(source)
        self.walker.preorder(tree, self.visitor)
        result = self.buffer.getvalue().strip()
        expected = expected.strip()
        self.assertEqual(expected, result)
            
    def testImport(self):
        source = """
from __future__ import absolute_import
from package import mod_name_1, mode_name_2
import mod_name
import mod_name_1, mod_name_2
from .level_1 import name
from ..level_2 import name
from star import *
from .level_1 import *
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testAdd(self):
        source = """
1 + 2
1 + 2 + 3
1 + 2 + 3 + 4
"""
        expected = """
(1 + 2)
((1 + 2) + 3)
(((1 + 2) + 3) + 4)
"""
        self.compileAndCheck(source, expected)
    
    def testMultiply(self):
        source = """
1 * 2
1 * 2 * 3
1 * 2 * 3 * 4
"""
        expected = """
(1 * 2)
((1 * 2) * 3)
(((1 * 2) * 3) * 4)
"""
        self.compileAndCheck(source, expected)
        
    def testSubtract(self):
        source = """
1 - 2
1 - 2 - 3
1 - 2 - 3 - 4
"""
        expected = """
(1 - 2)
((1 - 2) - 3)
(((1 - 2) - 3) - 4)
"""
        self.compileAndCheck(source, expected)
    
    def testDivide(self):
        source = """
1 / 2
1 / 2 / 3
1 / 2 / 3 / 4
1 % 2
1 % 2 % 3
1 % 2 % 3 % 4
1 // 2
1 // 2 // 3
1 // 2 // 3 // 4
"""
        expected = """
(1 / 2)
((1 / 2) / 3)
(((1 / 2) / 3) / 4)
(1 % 2)
((1 % 2) % 3)
(((1 % 2) % 3) % 4)
(1 // 2)
((1 // 2) // 3)
(((1 // 2) // 3) // 4)
"""
        self.compileAndCheck(source, expected)
    
    def testArithmetic(self):
        source = """
1 + 2 - 3 * 4 / 5 // 6 ** 7
7 ** 6 // 5 / 4 * 3 - 2 + 1
"""
        expected = """
((1 + 2) - (((3 * 4) / 5) // (6 ** 7)))
((((((7 ** 6) // 5) / 4) * 3) - 2) + 1)
"""
        self.compileAndCheck(source, expected)
    
    def testExpression(self):
        source = """
1 + (2 - 3) * 4 / 5 // 6 ** 7
7 ** 6 // (5 / 4) * 3 - 2 + 1
"""
        expected = """
(1 + ((((2 - 3) * 4) / 5) // (6 ** 7)))
(((((7 ** 6) // (5 / 4)) * 3) - 2) + 1)
"""
        self.compileAndCheck(source, expected)
        
    def testAssignment(self):
        source = """
a = a
a = b = c
a, b = c
a, b = b, a
a, b, c = c, b, a
a, b, c, d = d, c, b, a
a, b = [a, b]
a, b, c = [a, b, c]
a, b, c, d = [a, b, c, d]
"""
        expected = """
a = a
a = b = c
(a, b) = c
(a, b) = (b, a)
(a, b, c) = (c, b, a)
(a, b, c, d) = (d, c, b, a)
(a, b) = [a, b]
(a, b, c) = [a, b, c]
(a, b, c, d) = [a, b, c, d]
"""
        self.compileAndCheck(source, expected)

    def testUnpack(self):
        source = """(a[0], a[1]), a[2] = (c, d)
a[0], (a[1], a[2]) = (c, d)"""
        expected = """((a[0], a[1]), a[2]) = (c, d)
(a[0], (a[1], a[2])) = (c, d)"""
        self.compileAndCheck(source, expected)
    
    def testFunction(self):
        source = """
def func():
    pass

def func(arg):
    pass

def func(*varargs):
    pass

def func(**kwargs):
    pass

def func(arg, *varargs):
    pass

def func(arg, **kwargs):
    pass

def func(*varargs, **kwargs):
    pass

def func(arg, *varargs, **kwargs):
    pass

def func(default=default):
    pass

def func(default=default, *varargs):
    pass

def func(default=default, **kwargs):
    pass

def func(default=default, *varargs, **kwargs):
    pass

def func(arg, default=default):
    pass

def func(arg, default=default, *varargs):
    pass

def func(arg, default=default, **kwargs):
    pass

def func(arg, default=default, *varargs, **kwargs):
    pass

def func(arg1=1, arg2=2):
    pass

def func(arg1=1, arg2=2, *varargs):
    pass

def func(arg1=1, arg2=2, *varargs, **kwargs):
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testReturn(self):
        source = """
return 1
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testAnd(self):
        source = """
a and b
1 and 2 and 3
you and me and the_dog_named_Boo
"""
        expected = """
(a and b)
(1 and 2 and 3)
(you and me and the_dog_named_Boo)
"""
        self.compileAndCheck(source, expected)
    
    def testOr(self):
        source = """
a or b
1 or 2 or 3
1 or 2 or 3 or 4
"""
        expected = """
(a or b)
(1 or 2 or 3)
(1 or 2 or 3 or 4)
"""
        self.compileAndCheck(source, expected)
    
    def testNot(self):
        source = """
not a
b = not a
"""
        expected = source
        self.compileAndCheck(expected, source)
    
    def testInvert(self):
        source = """
~12345678
~0
"""
        expected = source
        self.compileAndCheck(expected, source)
    
    def testBitxor(self):
        source = """
a ^ b
a ^ b ^ c
a ^ b ^ c ^ d
"""
        expected = """
(a ^ b)
(a ^ b ^ c)
(a ^ b ^ c ^ d)
"""
        self.compileAndCheck(source, expected)
        
    def testBitand(self):
        source = """
a & b
a & b & c
a & b & c & d
"""
        expected = """
(a & b)
(a & b & c)
(a & b & c & d)
"""
        self.compileAndCheck(source, expected)
    
    def testBitor(self):
        source = """
a | b
a | b | c
a | b | c | d
"""
        expected = """
(a | b)
(a | b | c)
(a | b | c | d)
"""
        self.compileAndCheck(source, expected)
    
    def testUnaryAdd(self):
        source = """
+1
+1 + 2
+(1 + 2)
"""
        expected = """
+1
(+1 + 2)
+(1 + 2)
"""
        self.compileAndCheck(source, expected)
    
    def testUnarySubtract(self):
        source = """
-1
-1 + 2
-(1 + 2)
"""
        expected = """
-1
(-1 + 2)
-(1 + 2)
"""
        self.compileAndCheck(source, expected)
    
    def testTuple(self):
        source = """
1,
1, 2
1, 2, 3
"""
        expected = """
(1, )
(1, 2)
(1, 2, 3)
"""
        self.compileAndCheck(source, expected)
    
    def testList(self):
        source = """
[1]
[1, 2]
[1, 2, 3]
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testAssignAttribute(self):
        source = """
a.b = c
a.b.c = d
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testAssignList(self):
        source = """
[a, b] = [c, d]
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testAssert(self):
        source = """
assert 1
assert 1, 2
assert hello(world), goodbye(earth)
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testAugmentedAssignment(self):
        source = """
a += 1
a -= 1
a *= 1
a /= 1
a //= 1
a **= 1
a ^= 1
a &= 1
a |= 1
a <<= 1
a >>= 1
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testShift(self):
        source = """
a << b
a << b << c
a >> b
a >> b >> c
"""
        expected = """
(a << b)
((a << b) << c)
(a >> b)
((a >> b) >> c)
"""
        self.compileAndCheck(source, expected)
        
    def testBackquote(self):
        source = """
`1`
`'abc'`
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testCompare(self):
        source = """
a < b
a <= b
a >= b
a > b
a < b < c
a > b > c
a == b
a != b
a is b
a is not b
a in b
a not in b
"""
        expected = """
(a < b)
(a <= b)
(a >= b)
(a > b)
(a < b < c)
(a > b > c)
(a == b)
(a != b)
(a is b)
(a is not b)
(a in b)
(a not in b)
"""
        self.compileAndCheck(source, expected)
    
    def testBreakContinue(self):
        source = """
break
continue
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testExec(self):
        source = """
exec a
exec a in locals
exec a in locals, globals
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testFor(self):
        source = '''
for a in list:
    pass

for b in list:
    pass
else:
    pass
'''
        expected = source
        self.compileAndCheck(source, expected)
    
    def testWhile(self):
        source = """
while True:
    pass

while False:
    pass
else:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testYield(self):
        source = """
yield abc
a = yield d
b = (yield d) and c
c = yield a and b
"""
        expected = """
(yield abc)
a = (yield d)
b = ((yield d) and c)
c = (yield (a and b))
"""
        self.compileAndCheck(source, expected)
    
    def testWith(self):
        source = """
from __future__ import with_statement
with abc:
    pass

with open('blah', 'r') as f:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testTryFinally(self):
        source = """
try:
    pass
finally:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testTryExcept(self):
        source = """
try:
    pass
except a:
    pass
except b, c:
    pass
except (a, b, c), c:
    pass
except:
    pass

try:
    pass
except:
    pass
else:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testTryExceptFinally(self):
        source = """
try:
    try:
        pass
    except:
        pass

finally:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testIf(self):
        source = """
if a:
    pass

if b:
    pass
else:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testElif(self):
        source = """
if None:
    pass
elif None:
    pass
else:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
        
    def testSubscript(self):
        source = """
a[0] = b
a[0, 1] = c
a[...] = c
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testNone(self):
        source = """
from __future__ import with_statement
None
a = None
(a == None)
if None:
    pass

while None:
    pass

with None:
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
        
    def testSlice(self):
        source = """
a[0:] = b
a[:1] = b
a[0:1] = b
a[::] = b
a[::1] = b
a[0::1] = b
a[:1:-1] = b
a[0:0:-1] = b
"""
        expected = """
a[0 : ] = b
a[ : 1] = b
a[0 : 1] = b
a[ : : ] = b
a[ : : 1] = b
a[0 : : 1] = b
a[ : 1 : -1] = b
a[0 : 0 : -1] = b
"""
        self.compileAndCheck(source, expected)
    
    def testRaise(self):
        source = """
raise
raise a
raise a, b
raise a, b, c
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testListComp(self):
        source = """
[x for x in range(256)]
[x for x in range(256) if ((x % 2) == 1)]
[x for x in range(256) if ((x % 2) == 1) for y in range(256)]
[x for x in range(256) if ((x % 2) == 1) for y in range(256) if ((y % 2) != 1)]
[(x, y) for x in range(256)]
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testLambda(self):
        source = """
lambda x: x
lambda x, y: 1
lambda x, y: lambda z: u
lambda x, y=1: x
lambda x, y=1, z=2: z
lambda x=1: x
lambda x=1, y=2: z
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testGlobal(self):
        source = """
global a
global a, b
global a, b, c
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testGeneratorExpression(self):
        source = """
(x for x in range(256))
(x for x in range(256) if x)
(x for x in range(256) if x for y in range(256))
(x for x in range(256) if x for y in range(256) if not y)
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testFunctionDecorator(self):
        source = """
@hello
def func():
    pass

@hello
@world
def func():
    pass

@hello(a)
def func():
    pass

@hello(hello(b))
def func():
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testDict(self):
        source = """
{}
{1 : 2}
{'a' : 2}
{'a' : 'b'}
{1 : 2, 3 : 4}
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testClass(self):
        source = """
class a:
    pass

class a(object):
    pass

class a(b, c):
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
    def testCallFunc(self):
        source = """
a()
a(1)
a(1, 2)
a(*a)
a(1, *a)
a(1, 2, *a)
a(a=1)
a(1, a=1)
a(1, 2, a=1)
a(a=1, b=2)
a(1, a=1, b=2)
a(**a)
a(1, **a)
a(1, 2, **a)
a(a=b, **a)
a(1, a=b, **a)
a(1, *a, **a)
a(1, a=b, *a, **a)
"""
        self.compileAndCheck(source, source)

    def testPrint(self):
        source = """print a,
print a
print a, b,
print a, b
print >> f, a,
print >> f, a
print >> f, a, b,
print >> f, a, b
"""
        self.compileAndCheck(source, source)

    def testDoc(self):
        source = """
'module doc'
class abc:
    'class doc'
    pass

def func():
    'func doc'
    pass
"""
        expected = source
        self.compileAndCheck(source, expected)
    
if __name__ == "__main__":
    unittest.main()
