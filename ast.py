from __future__ import absolute_import
from __future__ import with_statement

from typecheck import accepts, returns, Self, Class


class Stack(object):
    
    def __init__(self):
        self.stack = []
    
    def push(self, value):
        self.stack.append(value)
    
    def pop(self):
        return self.stack.pop()


class Node(object):
    
    def __init__(self, value=None, children=None):
        if children == None:
            children = []
        self.children = children
        self.value = value
    
    def __str__(self):
        return "Node\n    " + ('\n'.join(str(x) for x in self.children)).replace('\n', '\n    ')

    
class LoadNode(Node):
    
    def __init__(self, value):
        Node.__init__(self, value, [])
    
    def __str__(self):
        return "%s" % repr(self.value)


class ExpressionNode(Node):
    
    def __init__(self, value):
        Node.__init__(self, value, [])
    
    def __str__(self):
        return "%s" % self.value


class StatementNode(Node):
    
    def __init__(self, value):
        Node.__init__(self, value)

    def __str__(self):
        return "%s" % self.value


class AssignmentStatementNode(StatementNode):
    """a = b"""
    
    def __init__(self, name, expression):
        Node.__init__(self, name, [expression])
    
    def __str__(self):
        return "%s = %s" % (self.value, self.children[0])


class ReturnStatementNode(StatementNode):
    
    def __init__(self, value):
        Node.__init__(self, value, [])
    
    def __str__(self):
        return "return %s" % self.value
