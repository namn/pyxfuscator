from block import Block, Instruction, Label
from types import CodeType
from sys import stdout
import opcode

def _visit_inst(inst, out):
    assert isinstance(inst, Instruction)
    s = "%5d %s" % (inst.position, opcode.opname[inst.opcode].ljust(20))
    out.write(s)
    if inst.oparg != Instruction.NO_ARG:
        oparg = inst.oparg
        if isinstance(oparg, Label):
            oparg = oparg.position
        s = " %s" % repr(oparg).replace('\\', '\\\\').replace('"', '\\"').replace('[', '\\[').replace(']', '\\]')
        out.write(s)

def _visit_block(block, out):
    assert isinstance(block, Block)
    out.write('block_%08X[shape=box, fontname=Courier, fontsize=12, label="' % id(block))
    for inst in block.insts:
        _visit_inst(inst, out)
        out.write('\\l')
    out.write('"];\n')
    
def _visit_code(code, out):
    assert isinstance(code, CodeType)
    blocks = Block.get_blocks(code)
    out.write('subgraph cluster_code_%08X {\n' % id(code))
    for block in blocks:
        _visit_block(block, out)
        for target, type in block.out_links:
            out.write('block_%08X -> block_%08X' % (id(block), id(target)))
            if type is True:
                out.write('[color="green"]')
            elif type is False:
                out.write('[color="red"]')
            out.write(';\n')
    out.write('label="code_%08X"\n};\n' % id(code))
    for const in code.co_consts:
        if isinstance(const, CodeType):
            # _visit_code(const, out)
            pass
        
def code2dot(code, out=stdout):
    out.write('digraph {\n')
    _visit_code(code, out)
    out.write('};\n')
