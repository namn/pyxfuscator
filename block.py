from __future__ import absolute_import
from __future__ import with_statement

from typecheck import accepts, returns, Self, Class, IsOneOf
from types import CodeType, NoneType

import uuid
import opcode
import dis


class Instruction(object):

    NO_ARG = object()

    @accepts(Self(), int, object, int)
    def __init__(self, opcode, oparg=NO_ARG, position=0):
        self.opcode = opcode
        self.oparg = oparg
        self.position = position
        self.block = None

    def __str__(self):
        s = "%8d %s" % (self.position, opcode.opname[self.opcode].ljust(20))
        if self.oparg != Instruction.NO_ARG:
            s += " %s" % str(self.oparg)
        return s


class Label(object):

    #@staticmethod
    #def generate_id():
        #return uuid.uuid4()

    @accepts(Self(), int, IsOneOf(list, NoneType))
    def __init__(self, position, sources=None):
        self.position = position
        if sources is None:
            sources = []
        self.sources = sources
        self.block = None

    @accepts(Self())
    @returns(str)
    def __str__(self):
        return "label 0x%08X at position %d" % (id(self), self.position)

    def __repr__(self):
        return self.__str__()


Block = Class("Block")
class Block(object):

    def __init__(self, insts=None):
        if insts is None:
            insts = []
        self.insts = insts
        self.in_links = set()
        self.out_links = set()
        self.label = None
    
    @staticmethod
    @accepts(CodeType)
    @returns([Label])
    def find_labels(co):
        '''Find all Label from code. Copied from dis.findlabels().'''
        code = co.co_code
        size = len(code)
        i = 0
        labels = []
        while i < size:
            c = code[i]
            op = ord(c)
            i = i+1
            if op >= opcode.HAVE_ARGUMENT:
                oparg = ord(code[i]) + ord(code[i+1]) * 256
                i = i+2
                position = -1
                if op in opcode.hasjrel:
                    position = i + oparg
                elif op in opcode.hasjabs:
                    position = oparg
                if position >= 0:
                    if position not in labels:
                        labels.append(Label(position))
        return labels

    @staticmethod
    @accepts([Block])
    @returns(NoneType)
    def _connect_blocks(blocks):
        '''Links the basic blocks together.'''
        for index, block in enumerate(blocks):
            last_inst = block.insts[-1]
            if type(last_inst.oparg) is Label:
                target_label = last_inst.oparg
                target_block = target_label.block
                if opcode.opname[last_inst.opcode] in ('JUMP_IF_FALSE', 'JUMP_IF_TRUE'):
                    block.out_links.add((target_block, True))
                    block.out_links.add((blocks[index + 1], False))
                    target_block.in_links.add((block, True))
                    blocks[index + 1].in_links.add((block, False))
                elif opcode.opname[last_inst.opcode] in ('SETUP_EXCEPT', 'SETUP_FINALLY', 'SETUP_LOOP', 'FOR_ITER'):
                    if index < len(blocks) - 1:
                        next_block = blocks[index + 1]
                        block.out_links.add((next_block, None))
                        next_block.in_links.add((block, None))
                else:
                    block.out_links.add((target_block, None))
                    target_block.in_links.add((block, None))
            else:
                if index < len(blocks) - 1:
                    next_block = blocks[index + 1]
                    block.out_links.add((next_block, None))
                    next_block.in_links.add((block, None))
    
    @staticmethod
    @accepts(CodeType)
    @returns([Block])
    def get_blocks(co):
        '''Segments a Code Object into basic blocks.'''
        blocks = []
        current_block = None
        code = co.co_code
        labels = Block.find_labels(co)
        label = None
        n = len(code)
        i = 0
        extended_arg = 0
        free = None
        while i < n:
            c = code[i]
            op = ord(c)
            current_pos = i
            i = i+1
            if op >= opcode.HAVE_ARGUMENT:
                oparg = ord(code[i]) + ord(code[i+1]) * 256 + extended_arg
                extended_arg = 0
                i = i+2
                if op == opcode.EXTENDED_ARG:
                    extended_arg = oparg * 65536L
                    continue
                if op in opcode.hasconst:
                    inst = Instruction(op, co.co_consts[oparg], current_pos)
                elif op in opcode.hasname:
                    inst = Instruction(op, co.co_names[oparg], current_pos)
                elif op in opcode.hasjrel:
                    position = i + oparg
                    label = labels[position]
                    inst = Instruction(op, label, current_pos)
                elif op in opcode.haslocal:
                    inst = Instruction(op, co.co_varnames[oparg], current_pos)
                elif op in opcode.hascompare:
                    inst = Instruction(op, opcode.cmp_op[oparg], current_pos)
                elif op in opcode.hasfree:
                    if free is None:
                        free = co.co_cellvars + co.co_freevars
                    inst = Instruction(op, free[oparg], current_pos)
                else:
                    inst = Instruction(op, oparg, current_pos)
            else:
                inst = Instruction(op, Instruction.NO_ARG, current_pos)
            if current_block is None or current_pos in labels:
                current_block = Block()
                if current_pos in labels:
                    current_block.label = labels[current_pos]
                    current_block.label.block = current_block
                blocks.append(current_block)
            current_block.insts.append(inst)
            inst.block = current_block
            if type(inst.oparg) is Label:
                inst.oparg.sources.append(inst)
                current_block = None
        Block._connect_blocks(blocks)
        return blocks

    @staticmethod
    @accepts(CodeType)
    @returns({int : Label})
    def find_labels(co):
        '''Find all Label from code. Copied from dis.findlabels().'''
        code = co.co_code
        size = len(code)
        i = 0
        labels = {}
        while i < size:
            c = code[i]
            op = ord(c)
            i = i+1
            if op >= opcode.HAVE_ARGUMENT:
                oparg = ord(code[i]) + ord(code[i+1]) * 256
                i = i+2
                position = -1
                if op in opcode.hasjrel:
                    position = i + oparg
                elif op in opcode.hasjabs:
                    position = oparg
                if position >= 0:
                    if position not in labels:
                        labels[position] = Label(position)
        return labels

    @accepts(Self())
    @returns(str)
    def __str__(self):
        lines = []
        lines.append("Block %08X" % id(self))
        lines.append(str(self.label))
        lines.append("in %d out %d" % (len(self.in_links), len(self.out_links)))
        for inst in self.insts:
            lines.append(str(inst))
        return '\n'.join(lines)

    def __repr__(self):
        return self.__str__()
