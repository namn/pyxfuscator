from compiler import ast, transformer, visitor
import random
import sys


class PrettyPrinter(object):
    
    def __init__(self, out=sys.stdout, indent=4):
        self.out = out
        self.indent = indent
    
    def increaseIndent(self):
        self.current_indent += self.indent
    
    def decreaseIndent(self):
        self.current_indent -= self.indent
        
    def write(self, msg):
        self.out.write(msg)
    
    def writeLine(self, line):
        self.out.write(line)
        self.out.write('\n')
    
    def writeSequence(self, nodes):
        if not nodes:
            return
        for n in nodes[:-1]:
            self.visit(n)
            self.write(', ')
        self.visit(nodes[-1])
            
    def writeSequenceWithBrackets(self, nodes, left_bracket, right_bracket):
        self.write(left_bracket)
        self.writeSequence(nodes)
        if left_bracket == '(' and len(nodes) == 1:
            self.write(', ')
        self.write(right_bracket)

    def writeTuple(self, nodes):
        self.writeSequenceWithBrackets(nodes, '(', ')')
    
    def writeList(self, nodes):
        self.writeSequenceWithBrackets(nodes, '[', ']')
        
    def visitModule(self, node):
        self.current_indent = 0
        if node.doc:
            self.writeLine(repr(node.doc))
        self.visit(node.node)
    
    def visitStmt(self, node):
        for n in node.nodes:
            self.write(self.current_indent * ' ')
            self.visit(n)
            self.write('\n')

    def visitAssign(self, node):
        self.visit(node.nodes[0])
        for n in node.nodes[1 : ]:
            self.write(" = ")
            self.visit(n)
        self.write(" = ")
        self.visit(node.expr)
    
    def visitAssAttr(self, node):
        self.visit(node.expr)
        self.write('.')
        self.write(node.attrname)
    
    visitGetattr = visitAssAttr
    
    def visitAssList(self, node):
        self.writeList(node.nodes)
    
    def visitAssName(self, node):
        self.write(node.name)
    
    def visitAssTuple(self, node):
        self.writeTuple(node.nodes)
    
    def visitAugAssign(self, node):
        self.visit(node.node)
        self.write(' ')
        self.write(node.op)
        self.write(' ')
        self.visit(node.expr)
    
    def visitExpression(self, node):
        self.write('(')
        self.visit(node.node)
        self.write(')')
    
    def visitTuple(self, node):
        self.writeTuple(node.nodes)
    
    def visitList(self, node):
        self.writeList(node.nodes)
    
    def visitDict(self, node):
        self.write('{')
        for key, value in node.items[:1]:
            self.visit(key)
            self.write(' : ')
            self.visit(value)
        for key, value in node.items[1:]:
            self.write(', ')
            self.visit(key)
            self.write(' : ')
            self.visit(value)
        self.write('}')
    
    def visitAssert(self, node):
        self.write('assert ')
        self.visit(node.test)
        if node.fail:
            self.write(', ')
            self.visit(node.fail)
        
    def visitBinaryOperator(self, node, operator_string):
        self.write('(')
        self.visit(node.left)
        self.write(operator_string)
        self.visit(node.right)
        self.write(')')
        
    def visitAdd(self, node):
        self.visitBinaryOperator(node, " + ")
    
    def visitMul(self, node):
        self.visitBinaryOperator(node, " * ")
    
    def visitSub(self, node):
        self.visitBinaryOperator(node, " - ")
    
    def visitDiv(self, node):
        self.visitBinaryOperator(node, " / ")
    
    def visitFloorDiv(self, node):
        self.visitBinaryOperator(node, " // ")
    
    def visitMod(self, node):
        self.visitBinaryOperator(node, " % ")
    
    def visitPower(self, node):
        self.visitBinaryOperator(node, " ** ")
    
    def visitLeftShift(self, node):
        self.visitBinaryOperator(node, " << ")
    
    def visitRightShift(self, node):
        self.visitBinaryOperator(node, " >> ")
    
    def visitNot(self, node):
        self.write('not ')
        self.visit(node.expr)
    
    def visitInvert(self, node):
        self.write('~')
        self.visit(node.expr)
    
    def visitGlobal(self, node):
        self.write('global ')
        self.write(', '.join(node.names))
        
    def visitMultioperandOperator(self, node, operator_string):
        self.write('(')
        self.visit(node.nodes[0])
        for n in node.nodes[1:]:
            self.write(operator_string)
            self.visit(n)
        self.write(')')

    def visitAnd(self, node):
        self.visitMultioperandOperator(node, " and ")
    
    def visitOr(self, node):
        self.visitMultioperandOperator(node, " or ")
    
    def visitBitxor(self, node):
        self.visitMultioperandOperator(node, " ^ ")
    
    def visitBitand(self, node):
        self.visitMultioperandOperator(node, " & ")
    
    def visitBitor(self, node):
        self.visitMultioperandOperator(node, " | ")

    def visitUnaryAdd(self, node):
        self.write('+')
        self.visit(node.expr)
    
    def visitUnarySub(self, node):
        self.write('-')
        self.visit(node.expr)
    
    def visitBackquote(self, node):
        self.write('`')
        self.visit(node.expr)
        self.write('`')
    
    def visitEllipsis(self, node):
        self.write('...')
    
    def visitBreak(self, node):
        self.write("break")
    
    def visitContinue(self, node):
        self.write("continue")
    
    def visitExec(self, node):
        self.write("exec ")
        self.visit(node.expr)
        if node.locals:
            self.write(' in ')
            self.visit(node.locals)
        if node.globals:
            self.write(', ')
            self.visit(node.globals)
            
    def visitCompare(self, node):
        self.write('(')
        self.visit(node.expr)
        for op in node.ops:
            operator_string, expr = op
            self.write(' ')
            self.write(operator_string)
            self.write(' ')
            self.visit(expr)
        self.write(')')
            
    def visitConst(self, node):
        self.write(repr(node.value))
    
    def visitName(self, node):
        self.write(node.name)
    
    def visitPass(self, node):
        self.write("pass")
    
    def visitLambda(self, node):
        self.write('lambda ')
        plain_arg_count = len(node.argnames) - len(node.defaults)
        self.write(', '.join(node.argnames[:plain_arg_count]))
        idx = plain_arg_count
        while idx < len(node.argnames):
            if idx > 0:
                self.write(', ')
            self.write(node.argnames[idx])
            self.write('=')
            self.visit(node.defaults[idx - plain_arg_count])
            idx += 1
        self.write(': ')
        self.visit(node.code)
    
    def visitClass(self, node):
        self.write('class ')
        self.write(node.name)
        if node.bases:
            self.write('(')
            self.writeSequence(node.bases)
            self.write(')')
        self.writeLine(':')
        self.increaseIndent()
        if node.doc:
            self.write(self.current_indent * ' ')
            self.writeLine(repr(node.doc))
        self.visit(node.code)
        self.decreaseIndent()
    
    def visitDecorators(self, node):
        for deco in node.nodes:
            self.write('@')
            self.visit(deco)
            self.write('\n')
            self.write(self.current_indent * ' ')
    
    def visitFunction(self, node):
        if node.decorators:
            self.visit(node.decorators)
        self.write("def ")
        self.write(node.name)
        self.write('(')
        plain_arg_count = len(node.argnames) - len(node.defaults)
        if node.kwargs:
            plain_arg_count -= 1
        if node.varargs:
            plain_arg_count -= 1
        self.write(', '.join(node.argnames[:plain_arg_count]))
        idx = plain_arg_count
        if node.defaults:
            if idx > 0:
                self.write(', ')
            for i, default in enumerate(node.defaults):
                self.write(node.argnames[idx])
                self.write('=')
                self.visit(default)
                idx += 1
                if i < len(node.defaults) - 1:
                    self.write(', ')
        if node.varargs:
            if idx > 0:
                self.write(', ')
            self.write('*')
            self.write(node.argnames[idx])
            idx += 1
        if node.kwargs:
            if idx > 0:
                self.write(', ')
            self.write('**')
            self.write(node.argnames[idx])
            idx += 1
        self.write(')')
        self.writeLine(':')
        self.increaseIndent()
        if node.doc:
            self.write(self.current_indent * ' ')
            self.writeLine(repr(node.doc))
        self.visit(node.code)
        self.decreaseIndent()
    
    def visitPrintnl(self, node):
        self.write("print ")
        if node.dest:
            self.write(">> ")
            self.visit(node.dest)
            self.write(", ")
        self.writeSequence(node.nodes)
 
    def visitPrint(self, node):
        self.visitPrintnl(node)
        self.write(',')
    
    def visitDiscard(self, node):
        self.visit(node.expr)
    
    def visitCallFunc(self, node):
        self.visit(node.node)
        self.write('(')
        self.writeSequence(node.args)
        if node.star_args:
            if node.args:
                self.write(', ')
            self.write('*')
            self.visit(node.star_args)
        if node.dstar_args:
            if node.args or node.star_args:
                self.write(', ')
            self.write('**')
            self.visit(node.dstar_args)
        self.write(')')
    
    def visitKeyword(self, node):
        self.write(node.name)
        self.write('=')
        self.visit(node.expr)
        
    def visitFrom(self, node):
        self.write('from ')
        self.write(node.level * '.')
        self.write(node.modname)
        self.write(' import ')
        self.write(', '.join(x[0] for x in node.names))
    
    def visitImport(self, node):
        self.write('import ')
        self.write(', '.join(x[0] for x in node.names))
    
    def visitReturn(self, node):
        self.write('return ')
        self.visit(node.value)
    
    def visitYield(self, node):
        self.write('(yield ')
        self.visit(node.value)
        self.write(')')
    
    def visitRaise(self, node):
        self.write('raise')
        if node.expr1:
            self.write(' ')
            self.visit(node.expr1)
        if node.expr2:
            self.write(', ')
            self.visit(node.expr2)
        if node.expr3:
            self.write(', ')
            self.visit(node.expr3)
    
    def visitGenExpr(self, node):
        self.write('(')
        self.visit(node.code)
        self.write(')')
    
    def visitGenExprInner(self, node):
        self.visit(node.expr)
        for qualifier in node.quals:
            self.write(' ')
            self.visit(qualifier)
    
    def visitGenExprFor(self, node):
        self.write('for ')
        self.visit(node.assign)
        self.write(' in ')
        self.visit(node.iter)
        for if_phrase in node.ifs:
            self.write(' ')
            self.visit(if_phrase)
    
    def visitListComp(self, node):
        self.write('[')
        self.visit(node.expr)
        for qualifier in node.quals:
            self.write(' ')
            self.visit(qualifier)
        self.write(']')
    
    def visitListCompFor(self, node):
        self.write('for ')
        self.visit(node.assign)
        self.write(' in ')
        self.visit(node.list)
        for if_phrase in node.ifs:
            self.write(' ')
            self.visit(if_phrase)
    
    def visitListCompIf(self, node):
        self.write('if ')
        self.visit(node.test)
    visitGenExprIf = visitListCompIf
    
    def visitSubscript(self, node):
        self.visit(node.expr)
        self.write('[')
        self.writeSequence(node.subs)
        self.write(']')
    
    def isConstNone(self, node):
        return (node is None) or \
               (isinstance(node, ast.Const) and (node.value is None))
    
    def visitSlice(self, node):
        self.visit(node.expr)
        self.write('[')
        if not self.isConstNone(node.lower):
            self.visit(node.lower)
        self.write(' : ')
        if not self.isConstNone(node.upper):
            self.visit(node.upper)
        self.write(']')

    def visitSliceobj(self, node):
        start, end, step = node.nodes
        if not self.isConstNone(start):
            self.visit(start)
        self.write(' :')
        if not self.isConstNone(end):
            self.write(' ')
            self.visit(end)
        self.write(' : ')
        if not self.isConstNone(step):
            self.visit(step)
    
    def visitIf(self, node):
        def writeConditionBody(condition, body):
            self.visit(condition)
            self.writeLine(":")
            self.increaseIndent()
            self.visit(body)
            self.decreaseIndent()
        self.write("if ")
        condition, body = node.tests[0]
        writeConditionBody(condition, body)
        for test in node.tests[1:]:
            self.write(self.current_indent * ' ')
            self.write('elif ')
            condition, body = test
            writeConditionBody(condition, body)
        if node.else_:
            self.write(self.current_indent * ' ')
            self.writeLine('else:')
            self.increaseIndent()
            self.visit(node.else_)
            self.decreaseIndent()
            
    def visitFor(self, node):
        self.write('for ')
        self.visit(node.assign)
        self.write(' in ')
        self.visit(node.list)
        self.writeLine(':')
        self.increaseIndent()
        self.visit(node.body)
        self.decreaseIndent()
        if node.else_:
            self.write(self.current_indent * ' ')
            self.writeLine("else:")
            self.increaseIndent()
            self.visit(node.else_)
            self.decreaseIndent()
    
    def visitWhile(self, node):
        self.write('while ')
        self.visit(node.test)
        self.writeLine(":")
        self.increaseIndent()
        self.visit(node.body)
        self.decreaseIndent()
        if node.else_:
            self.write(self.current_indent * ' ')
            self.writeLine("else:")
            self.increaseIndent()
            self.visit(node.else_)
            self.decreaseIndent()
    
    def visitWith(self, node):
        self.write('with ')
        self.visit(node.expr)
        if node.vars:
            self.write(' as ')
            self.visit(node.vars)
        self.writeLine(":")
        self.increaseIndent()
        self.visit(node.body)
        self.decreaseIndent()
    
    def visitTryFinally(self, node):
        self.writeLine("try:")
        self.increaseIndent()
        self.visit(node.body)
        self.decreaseIndent()
        self.write(self.current_indent * ' ')
        self.writeLine("finally:")
        self.increaseIndent()
        self.visit(node.final)
        self.decreaseIndent()
    
    def visitTryExcept(self, node):
        self.writeLine("try:")
        self.increaseIndent()
        self.visit(node.body)
        self.decreaseIndent()
        for handler in node.handlers:
            self.write(self.current_indent * ' ')
            self.write('except')
            exceptions, variable, body = handler
            if exceptions:
                self.write(' ')
                self.visit(exceptions)
            if variable:
                self.write(', ')
                self.visit(variable)
            self.writeLine(":")
            self.increaseIndent()
            self.visit(body)
            self.decreaseIndent()
        if node.else_:
            self.write(self.current_indent * ' ')
            self.writeLine('else:')
            self.increaseIndent()
            self.visit(node.else_)
            self.decreaseIndent()
