from __future__ import absolute_import
from __future__ import with_statement

from cStringIO import StringIO
from compiler import visitor, transformer, ast
from decompyle import Decompiler25, DecompileHint, HintEnum
from pyxfuscator import PrettyPrinter
import unittest
import dis
import sys
import logging


class TestDecompiler25(unittest.TestCase):
    
    def compileAndCheck(self, src, expected=None, hints=None):
        global test_count
        test_count += 1
        if expected is None:
            expected = src
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, hints)
        try:
            tree = decompiler.decompile()
            tree = ast.Module(None, tree)
        except:
            logging.exception("cannot decompile %s", src)
            print "parsed tree:"
            print transformer.parse(src)
            print dis.dis(code)
            self.fail()
        out = StringIO()
        walker = visitor.ASTVisitor()
        pp = PrettyPrinter(out)
        try:
            walker.preorder(tree, pp)
        except:
            logging.exception("cannot prettify")
            print "parsed tree:"
            print transformer.parse(src)
            print "decompiled tree:"
            print tree
            print dis.dis(code)
            self.fail()
        result = out.getvalue()
        out.close()
        expected = expected.strip()
        result = result.strip()
        if expected.strip() != result.strip():
            print result
            print "is not identical to"
            print expected
            print "debug info:"
            print "parsed tree:"
            print transformer.parse(src)
            print "decompiled tree:"
            print tree
            #print "stack:"
            #print decompiler.stack
            print dis.dis(code)
            self.fail()
    
    def testAssignment(self):
        src = """a = b"""
        self.compileAndCheck(src)
        src = """a = 'c'"""
        self.compileAndCheck(src)
        src = """a = 1"""
        self.compileAndCheck(src)
        src = "a = 999999999999999999L"
        self.compileAndCheck(src)
        src = "a = 123.455"
        self.compileAndCheck(src)
        src = "a = (1, )"
        self.compileAndCheck(src)
        src = "a = (1, 2)"
        self.compileAndCheck(src)
        src = "a = []"
        self.compileAndCheck(src)
        src = "a = [1]"
        self.compileAndCheck(src)
        src = "a = [1, 2]"
        self.compileAndCheck(src)
        src = "a = [1, (2, 3)]"
        self.compileAndCheck(src)
        src = "a = b = c"
        self.compileAndCheck(src)
        
    def testAdd(self):
        src = "1 + 2"
        exp = "3"
        self.compileAndCheck(src, exp)
        src = "a + 1"
        exp = "(a + 1)"
        self.compileAndCheck(src, exp)
        src = "a + b"
        exp = "(a + b)"
        self.compileAndCheck(src, exp)
        src = "1 + 2\na + 1\na + b"
        exp = "3\n(a + 1)\n(a + b)"
        self.compileAndCheck(src, exp)
    
    def testSub(self):
        src = "1 - 2"
        exp = "-1"
        self.compileAndCheck(src, exp)
        src = "a - 1"
        exp = "(a - 1)"
        self.compileAndCheck(src, exp)
        src = "a - b"
        exp = "(a - b)"
        self.compileAndCheck(src, exp)
        src = "1 - 2\na - 1\na - b"
        exp = "-1\n(a - 1)\n(a - b)"
        self.compileAndCheck(src, exp)

    def testMul(self):
        src = "1 * 2"
        exp = "2"
        self.compileAndCheck(src, exp)
        src = "a * 1"
        exp = "(a * 1)"
        self.compileAndCheck(src, exp)
        src = "a * b"
        exp = "(a * b)"
        self.compileAndCheck(src, exp)
        src = "1 * 2\na * 1\na * b"
        exp = "2\n(a * 1)\n(a * b)"
        self.compileAndCheck(src, exp)
    
    def testDiv(self):
        src = "1 / 2"
        exp = "(1 / 2)"
        self.compileAndCheck(src, exp)
        src = "a / 1"
        exp = "(a / 1)"
        self.compileAndCheck(src, exp)
        src = "a / b"
        exp = "(a / b)"
        self.compileAndCheck(src, exp)
        src = "1 / 2\na / 1\na / b"
        exp = "(1 / 2)\n(a / 1)\n(a / b)"
        self.compileAndCheck(src, exp)
    
    def testFloorDiv(self):
        src = "1 // 2"
        exp = "0"
        self.compileAndCheck(src, exp)
        src = "a // 1"
        exp = "(a // 1)"
        self.compileAndCheck(src, exp)
        src = "a // b"
        exp = "(a // b)"
        self.compileAndCheck(src, exp)
        src = "1 // 2\na // 1\na // b"
        exp = "0\n(a // 1)\n(a // b)"
        self.compileAndCheck(src, exp)
    
    def testMod(self):
        src = "1 % 2"
        exp = "1"
        self.compileAndCheck(src, exp)
        src = "a % 1"
        exp = "(a % 1)"
        self.compileAndCheck(src, exp)
        src = "a % b"
        exp = "(a % b)"
        self.compileAndCheck(src, exp)
        src = "1 % 2\na % 1\na % b"
        exp = "1\n(a % 1)\n(a % b)"
        self.compileAndCheck(src, exp)
    
    def testShift(self):
        src = "a << 1"
        exp = "(a << 1)"
        self.compileAndCheck(src, exp)
        src = "a << 2"
        exp = "(a << 2)"
        self.compileAndCheck(src, exp)
        src = "a << b"
        exp = "(a << b)"
        self.compileAndCheck(src, exp)
        src = "a << b << c"
        exp = "((a << b) << c)"
        self.compileAndCheck(src, exp)
        src = "a >> 1"
        exp = "(a >> 1)"
        self.compileAndCheck(src, exp)
        src = "a >> 2"
        exp = "(a >> 2)"
        self.compileAndCheck(src, exp)
        src = "a >> b"
        exp = "(a >> b)"
        self.compileAndCheck(src, exp)
        src = "a >> b >> c"
        exp = "((a >> b) >> c)"
        self.compileAndCheck(src, exp)
    
    def testPower(self):
        src = "1 ** 2"
        exp = "1"
        self.compileAndCheck(src, exp)
        src = "a ** 1"
        exp = "(a ** 1)"
        self.compileAndCheck(src, exp)
        src = "a ** b"
        exp = "(a ** b)"
        self.compileAndCheck(src, exp)
        src = "1 ** 2\na ** 1\na ** b"
        exp = "1\n(a ** 1)\n(a ** b)"
        self.compileAndCheck(src, exp)

    def testLogicOperator(self):
        src = "a & 1"
        exp = "(a & 1)"
        self.compileAndCheck(src, exp)
        src = "a & b"
        exp = "(a & b)"
        self.compileAndCheck(src, exp)
        src = "a & b & c"
        exp = "((a & b) & c)"
        self.compileAndCheck(src, exp)
        src = "a | 1"
        exp = "(a | 1)"
        self.compileAndCheck(src, exp)
        src = "a | b"
        exp = "(a | b)"
        self.compileAndCheck(src, exp)
        src = "a | b | c"
        exp = "((a | b) | c)"
        self.compileAndCheck(src, exp)
        src = "a ^ 1"
        exp = "(a ^ 1)"
        self.compileAndCheck(src, exp)
        src = "a ^ b"
        exp = "(a ^ b)"
        self.compileAndCheck(src, exp)
        src = "a ^ b ^ c"
        exp = "((a ^ b) ^ c)"
        self.compileAndCheck(src, exp)
        
    def testCallFunc(self):
        src = "a()"
        self.compileAndCheck(src)
        src = "a(1)"
        self.compileAndCheck(src)
        src = "a('b')"
        self.compileAndCheck(src)
        src = "a(b)"
        self.compileAndCheck(src)
        src = "a(1, 2)"
        self.compileAndCheck(src)
        src = "a(1, a=2)"
        self.compileAndCheck(src)
        src = "a(1, a=b)"
        self.compileAndCheck(src)
        src = "a(1, a=b, a=3)"
        self.compileAndCheck(src)
        src = "a(1, a=b, a=c)"
        self.compileAndCheck(src)
        src = "a(*a)"
        self.compileAndCheck(src)
        src = "a(1, *a)"
        self.compileAndCheck(src)
        src = "a(1, 2, *a)"
        self.compileAndCheck(src)
        src = "a(1, 2, a=1, *a)"
        self.compileAndCheck(src)
        src = "a(1, 2, a=1, *a)"
        self.compileAndCheck(src)
        src = "a(1, 2, a=1, a=b, *a)"
        self.compileAndCheck(src)
        src = "a(**b)"
        self.compileAndCheck(src)
        src = "a(1, **b)"
        self.compileAndCheck(src)
        src = "a(a=2, **b)"
        self.compileAndCheck(src)
        src = "a(1, a=2, **b)"
        self.compileAndCheck(src)
        src = "a(*a, **b)"
        self.compileAndCheck(src)
        src = "a(1, *a, **b)"
        self.compileAndCheck(src)
        src = "a(a=2, *a, **b)"
        self.compileAndCheck(src)
        src = "a(1, a=2, *a, **b)"
        self.compileAndCheck(src)
    
    def testArithmeticExpression(self):
        src = "a + b + c"
        exp = "((a + b) + c)"
        self.compileAndCheck(src, exp)
        src = "a - b + c"
        exp = "((a - b) + c)"
        self.compileAndCheck(src, exp)
        src = "a * b + c"
        exp = "((a * b) + c)"
        self.compileAndCheck(src, exp)
        src = "a + b * c"
        exp = "(a + (b * c))"
        self.compileAndCheck(src, exp)
        src = "a * b / c"
        exp = "((a * b) / c)"
        self.compileAndCheck(src, exp)
        src = "a * b / a(c)"
        exp = "((a * b) / a(c))"
        self.compileAndCheck(src, exp)
    
    def testPrint(self):
        src = "print abc"
        self.compileAndCheck(src)
        src = "print abc,"
        self.compileAndCheck(src)
        src = "print a, b"
        exp = "print a,\nprint b"
        self.compileAndCheck(src, exp)
        src = "print a, b,"
        exp = "print a,\nprint b,"
        self.compileAndCheck(src, exp)
        src = "print >> f, a,"
        self.compileAndCheck(src)
        src = "print >> f, a"
        self.compileAndCheck(src)
        src = "print >> f, a, b,"
        exp = "print >> f, a,\nprint >> f, b,"
        self.compileAndCheck(src, exp)
        src = "print >> f, a, b"
        exp = "print >> f, a,\nprint >> f, b"
        self.compileAndCheck(src, exp)
    
    def testExec(self):
        src = "exec a"
        self.compileAndCheck(src)
        src = "exec a in locals"
        self.compileAndCheck(src)
        src = "exec a in locals, globals"
        self.compileAndCheck(src)
    
    def testImport(self):
        src = "import a"
        self.compileAndCheck(src)
        src = "import name"
        self.compileAndCheck(src)
        src = "import a, b"
        exp = "import a\nimport b"
        self.compileAndCheck(src, exp)
        src = "import name1, name2"
        exp = "import name1\nimport name2"
        self.compileAndCheck(src, exp)
        src = "from a import b"
        self.compileAndCheck(src)
        src = "from a import b, c"
        self.compileAndCheck(src)
        src = "from a import *"
        self.compileAndCheck(src)
        src = "from a.b import c"
        self.compileAndCheck(src)
        src = "from a.b import c, d"
        self.compileAndCheck(src)
        src = "from a.b import *"
        self.compileAndCheck(src)
        src = "from a import b\nfrom a import c"
        self.compileAndCheck(src)
        src = "from a import b, c\nfrom a import d"
        self.compileAndCheck(src)
        src = "from __future__ import absolute_import\nfrom a import b"
        self.compileAndCheck(src)
        src = "from __future__ import absolute_import\nfrom .a import b"
        self.compileAndCheck(src)
        src = "from __future__ import absolute_import\nfrom ..a import b"
        self.compileAndCheck(src)
        src = "from __future__ import absolute_import\nfrom .a import b, c"
        self.compileAndCheck(src)
        src = "from __future__ import absolute_import\nfrom ..a import b, c"
        self.compileAndCheck(src)

    def testGetAttr(self):
        src = "a.b"
        self.compileAndCheck(src)
        src = "a.b.c"
        self.compileAndCheck(src)
        src = "a.b.c + a.b.d"
        exp = '(' + src + ')'
        self.compileAndCheck(src, exp)
    
    def testSetAttr(self):
        src = "a.b = 1"
        self.compileAndCheck(src)
        src = "a.b.c = d.e.f"
        self.compileAndCheck(src)
        src = "a.b = a + b"
        exp = "a.b = (a + b)"
        self.compileAndCheck(src, exp)
    
    def testTuple(self):
        src = "(1, )"
        self.compileAndCheck(src)
        src = "(1, 2)"
        self.compileAndCheck(src)
    
    def testList(self):
        src = "[]"
        self.compileAndCheck(src)
        src = "[1]"
        self.compileAndCheck(src)
        src = "[1, 2]"
        self.compileAndCheck(src)
    
    def testSubscr(self):
        src = "a[0]"
        self.compileAndCheck(src)
        src = "a[1]"
        self.compileAndCheck(src)
        src = "a[0, 1]"
        exp = "a[(0, 1)]"
        self.compileAndCheck(src, exp)
        src = "a[0, 1, 2]"
        exp = "a[(0, 1, 2)]"
        self.compileAndCheck(src, exp)
        src = "a[(0, 1)]"
        self.compileAndCheck(src)
        src = "a[(0, 1, 2)]"
        self.compileAndCheck(src)
        src = "a[(0, 1), 2]"
        exp = "a[((0, 1), 2)]"
        self.compileAndCheck(src, exp)
        src = "a[((0, 1), 2)]"
        self.compileAndCheck(src)
    
    def testStoreSubscr(self):
        src = "a[1] = b"
        self.compileAndCheck(src)
        src = "a[2] = (c + 1)"
        self.compileAndCheck(src)
        src = "a[3] = d[4]"
        self.compileAndCheck(src)
 
    def testCompare(self):
        src = "a < b"
        exp = "(a < b)"
        self.compileAndCheck(src, exp)
        src = "a <= b"
        exp = "(a <= b)"
        self.compileAndCheck(src, exp)
        src = "a == b"
        exp = "(a == b)"
        self.compileAndCheck(src, exp)
        src = "a != b"
        exp = "(a != b)"
        self.compileAndCheck(src, exp)
        src = "a >= b"
        exp = "(a >= b)"
        self.compileAndCheck(src, exp)
        src = "a > b"
        exp = "(a > b)"
        self.compileAndCheck(src, exp)
        src = "a is b"
        exp = "(a is b)"
        self.compileAndCheck(src, exp)
        src = "a is not b"
        exp = "(a is not b)"
        self.compileAndCheck(src, exp)
        src = "a in b"
        exp = "(a in b)"
        self.compileAndCheck(src, exp)
        src = "a not in b"
        exp = "(a not in b)"
        self.compileAndCheck(src, exp)
    
    def testSlice(self):
        src = "a[ : ]"
        self.compileAndCheck(src)
        src = "a[0 : ]"
        self.compileAndCheck(src)
        src = "a[ : 1]"
        self.compileAndCheck(src)
        src = "a[0 : 1]"
        self.compileAndCheck(src)
        src = "a[ : : ]"
        self.compileAndCheck(src)
        src = "a[ : : 1]"
        self.compileAndCheck(src)
        src = "a[0 : : 1]"
        self.compileAndCheck(src)
        src = "a[ : 1 : -1]"
        self.compileAndCheck(src)
        src = "a[0 : 0 : -1]"
        self.compileAndCheck(src)

    def testSliceAssign(self):
        src = "a[ : ] = b"
        self.compileAndCheck(src)
        src = "a[0 : ] = b"
        self.compileAndCheck(src)
        src = "a[ : 1] = b"
        self.compileAndCheck(src)
        src = "a[0 : 1] = b"
        self.compileAndCheck(src)
        src = "a[ : : ] = b"
        self.compileAndCheck(src)
        src = "a[ : : 1] = b"
        self.compileAndCheck(src)
        src = "a[0 : : 1] = b"
        self.compileAndCheck(src)
        src = "a[ : 1 : -1] = b"
        self.compileAndCheck(src)
        src = "a[0 : 0 : -1] = b"
        self.compileAndCheck(src)
    
    def testSwapAssign(self):
        src = "a = b\nb = a"
        self.compileAndCheck(src)
        src = "a, b = b, a"
        exp = "(a, b) = (b, a)"
        self.compileAndCheck(src, exp)
        src = "a.b = c.d\nc.d = a.b"
        self.compileAndCheck(src)
        src = "a.b, c.d = c.d, a.b"
        exp = "(a.b, c.d) = (c.d, a.b)"
        self.compileAndCheck(src, exp)
        src = "a, b, c = c, b, a"
        exp = "(a, b, c) = (c, b, a)"
        self.compileAndCheck(src, exp)
        src = "a.b, c.d, e.f = e.f, c.d, a.b"
        exp = "(a.b, c.d, e.f) = (e.f, c.d, a.b)"
        self.compileAndCheck(src, exp)
        src = "a, b, c, d = d, c, b, a"
        exp = "(a, b, c, d) = (d, c, b, a)"
        self.compileAndCheck(src, exp)
        src = "a.b, c.d, e.f, g.h = g.h, e.f, c.d, a.b"
        exp = "(a.b, c.d, e.f, g.h) = (g.h, e.f, c.d, a.b)"
        self.compileAndCheck(src, exp)
        src = "a[0], a[1] = a[1], a[0]"
        exp = "(a[0], a[1]) = (a[1], a[0])"
        self.compileAndCheck(src, exp)
    
    def testAugmentAssign(self):
        src = "a += 1"
        self.compileAndCheck(src)
        src = "a.b += 1"
        self.compileAndCheck(src)
        src = "a += 1 + b"
        exp = "a += (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b += 1 + b"
        exp = "a.b += (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a -= 1"
        self.compileAndCheck(src)
        src = "a.b -= 1"
        self.compileAndCheck(src)
        src = "a -= 1 + b"
        exp = "a -= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b -= 1 + b"
        exp = "a.b -= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a *= 1"
        self.compileAndCheck(src)
        src = "a.b *= 1"
        self.compileAndCheck(src)
        src = "a *= 1 + b"
        exp = "a *= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b *= 1 + b"
        exp = "a.b *= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a /= 1"
        self.compileAndCheck(src)
        src = "a.b /= 1"
        self.compileAndCheck(src)
        src = "a /= 1 + b"
        exp = "a /= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b /= 1 + b"
        exp = "a.b /= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a //= 1"
        self.compileAndCheck(src)
        src = "a.b //= 1"
        self.compileAndCheck(src)
        src = "a //= 1 + b"
        exp = "a //= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b //= 1 + b"
        exp = "a.b //= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a **= 1"
        self.compileAndCheck(src)
        src = "a.b **= 1"
        self.compileAndCheck(src)
        src = "a **= 1 + b"
        exp = "a **= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b **= 1 + b"
        exp = "a.b **= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a <<= 1"
        self.compileAndCheck(src)
        src = "a.b <<= 1"
        self.compileAndCheck(src)
        src = "a <<= 1 + b"
        exp = "a <<= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b <<= 1 + b"
        exp = "a.b <<= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a >>= 1"
        self.compileAndCheck(src)
        src = "a.b >>= 1"
        self.compileAndCheck(src)
        src = "a >>= 1 + b"
        exp = "a >>= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a.b >>= 1 + b"
        exp = "a.b >>= (1 + b)"
        self.compileAndCheck(src, exp)
        src = "a[0] += (c + d)"
        self.compileAndCheck(src)
    
    def testUnaryOperator(self):
        src = "a = (-1 + b)"
        self.compileAndCheck(src)
        src = "a = (+1 + b)"
        self.compileAndCheck(src)
    
    def testUnpack(self):
        src = "a, b = c, d"
        exp = "(a, b) = (c, d)"
        self.compileAndCheck(src, exp)
        src = "a, b, a1 = c, d, e"
        exp = "(a, b, a1) = (c, d, e)"
        self.compileAndCheck(src, exp)
        src = "a, b, c, d = d, c, b, a"
        exp = "(a, b, c, d) = (d, c, b, a)"
        self.compileAndCheck(src, exp)
        src = "a, b = c"
        exp = "(a, b) = c"
        self.compileAndCheck(src, exp)
        src = "a[0], a[1] = c"
        exp = "(a[0], a[1]) = c"
        self.compileAndCheck(src, exp)
        src = "(a[0], a[1]), a[2] = (c, d)"
        exp = "((a[0], a[1]), a[2]) = (c, d)"
        self.compileAndCheck(src, exp)
        src = "a[0], (a[1], a[2]) = (c, d)"
        exp = "(a[0], (a[1], a[2])) = (c, d)"
        self.compileAndCheck(src, exp)
        src = "a = b, c = d, e"
        exp = "a = (b, c) = (d, e)"
        self.compileAndCheck(src, exp)
    
    def testDict(self):
        src = "{}"
        self.compileAndCheck(src)
        # the below tests need store_subscr
        src = "{1 : 2}"
        self.compileAndCheck(src)
        src = "{1 : {}}"
        self.compileAndCheck(src)
        src = "{a : b}"
        self.compileAndCheck(src)
        src = "{'a' : 'b'}"
        self.compileAndCheck(src)
        src = "a = {1 : 2}"
        self.compileAndCheck(src)
        src = "{1 : 2, 3 : 4}"
        self.compileAndCheck(src)
        src = "a = {1 : 2, 3 : 4}\na[0] = 1"
        self.compileAndCheck(src)
        src = "[0, 1, {}, {1 : 2}]"
        self.compileAndCheck(src)

    def testAnd(self):
        src = "(a and b)"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        l = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 8)
        r = decompiler.decompile()
        hints = [DecompileHint(0, 9, ast.And([l, r]), HintEnum.HINT_STACK)]
        self.compileAndCheck(src, None, hints)
        
        src = "(a and b)"
        hint = DecompileHint(0, 9, None, HintEnum.HINT_AND). \
             add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT)). \
             add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
        
        src = "(a and b and c)"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 8)
        b = decompiler.decompile()
        decompiler = Decompiler25(code, None, 14, 15)
        c = decompiler.decompile()
        hints = [DecompileHint(0, 15, ast.And([a, b, c]), HintEnum.HINT_STACK)]
        self.compileAndCheck(src, None, hints)
        
        src = "(a and b and c)"
        hint = DecompileHint(0, 16, None, HintEnum.HINT_AND). \
             add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT)). \
             add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT)). \
             add_hint(DecompileHint(14, 16, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
    
    def test_or(self):
        src = "(a or b)"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        l = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 8)
        r = decompiler.decompile()
        hints = [DecompileHint(0, 9, ast.Or([l, r]), HintEnum.HINT_STACK)]
        self.compileAndCheck(src, None, hints)
        
        src = "(a or b)"
        hint = DecompileHint(0, 9, None, HintEnum.HINT_OR). \
             add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT)). \
             add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
        
        src = "(a or b or c)"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 8)
        b = decompiler.decompile()
        decompiler = Decompiler25(code, None, 14, 15)
        c = decompiler.decompile()
        hints = [DecompileHint(0, 15, ast.Or([a, b, c]), HintEnum.HINT_STACK)]
        self.compileAndCheck(src, None, hints)
        
        src = "(a or b or c)"
        hint = DecompileHint(0, 16, None, HintEnum.HINT_OR). \
             add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT)). \
             add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT)). \
             add_hint(DecompileHint(14, 16, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
    
    def test_and_or(self):
        src = "((a and b) or c)"
        hint = DecompileHint(0, 16, None, HintEnum.HINT_OR). \
             add_hint(DecompileHint(0, 12, None, HintEnum.HINT_AND). \
                 add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT)). \
                 add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT))). \
             add_hint(DecompileHint(14, 16, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
        
        src = "(a and (b or c))"
        hint = DecompileHint(0, 16, None, HintEnum.HINT_AND). \
             add_hint(DecompileHint(7, 16, None, HintEnum.HINT_OR). \
                 add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT)). \
                 add_hint(DecompileHint(14, 16, None, HintEnum.HINT_FRAGMENT))). \
             add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
    
    def test_or_and(self):
        src = "((a or b) and c)"
        hint = DecompileHint(0, 16, None, HintEnum.HINT_AND). \
             add_hint(DecompileHint(0, 12, None, HintEnum.HINT_OR). \
                 add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT)). \
                 add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT))). \
             add_hint(DecompileHint(14, 16, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
        
        src = "(a or (b and c))"
        hint = DecompileHint(0, 16, None, HintEnum.HINT_OR). \
             add_hint(DecompileHint(7, 16, None, HintEnum.HINT_AND). \
                 add_hint(DecompileHint(7, 9, None, HintEnum.HINT_FRAGMENT)). \
                 add_hint(DecompileHint(14, 16, None, HintEnum.HINT_FRAGMENT))). \
             add_hint(DecompileHint(0, 2, None, HintEnum.HINT_FRAGMENT))
        self.compileAndCheck(src, None, [hint])
        
    def test_if(self):
        src = "if a:\n    b\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 10)
        b = decompiler.decompile()
        hints = [DecompileHint(0, 14, ast.If([(a, b)], None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "if a:\n    b\n    c\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 14)
        bc = decompiler.decompile()
        hints = [DecompileHint(0, 18, ast.If([(a, bc)], None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "if a:\n    b\nelse:\n    c\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 10)
        b = decompiler.decompile()
        decompiler = Decompiler25(code, None, 15, 18)
        c = decompiler.decompile()
        hints = [DecompileHint(0, 18, ast.If([(a, b)], c), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "if a:\n    b\nelse:\n    c\n\nd\ne\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 0, 1)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 7, 10)
        b = decompiler.decompile()
        decompiler = Decompiler25(code, None, 15, 18)
        c = decompiler.decompile()
        hints = [DecompileHint(0, 18, ast.If([(a, b)], c), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)

    def test_while(self):
        src = "while a:\n    b\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 3, 4)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 10, 13)
        b = decompiler.decompile()
        hints = [DecompileHint(0, 18, ast.While(a, b, None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "while a:\n    b\n    c\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 3, 4)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 10, 17)
        bc = decompiler.decompile()
        hints = [DecompileHint(0, 22, ast.While(a, bc, None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "while (a and b):\n    c\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 3, 4)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 10, 11)
        b = decompiler.decompile()
        decompiler = Decompiler25(code, None, 17, 20)
        a = ast.And([a, b])
        c = decompiler.decompile()
        hints = [DecompileHint(0, 25, ast.While(a, c, None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "while (a and b):\n    c\nelse:\n    d\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 3, 4)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 10, 11)
        b = decompiler.decompile()
        a = ast.And([a, b])
        decompiler = Decompiler25(code, None, 17, 20)        
        c = decompiler.decompile()
        decompiler = Decompiler25(code, None, 26, 29)
        d = decompiler.decompile()
        hints = [DecompileHint(0, 29, ast.While(a, c, d), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "while (a and b):\n    c\nelse:\n    d\n\ne\nf\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, None, 3, 4)
        a = decompiler.decompile()
        decompiler = Decompiler25(code, None, 10, 11)
        b = decompiler.decompile()
        a = ast.And([a, b])
        decompiler = Decompiler25(code, None, 17, 20)        
        c = decompiler.decompile()
        decompiler = Decompiler25(code, None, 26, 29)
        d = decompiler.decompile()
        hints = [DecompileHint(0, 29, ast.While(a, c, d), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
    
    def test_for(self):
        src = "for a in b:\n    c\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, [DecompileHint(6, 7, None, HintEnum.HINT_PASS)], 3, 10)
        ab = decompiler.decompile().nodes[0]
        decompiler = Decompiler25(code, None, 13, 16)
        c = decompiler.decompile()
        hints = [DecompileHint(0, 20, ast.For(ab.nodes[0], ab.expr, c, None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
        src = "for (a, b) in c:\n    d\n"
        code = compile(src, "<string>", "exec")
        decompiler = Decompiler25(code, [DecompileHint(6, 7, None, HintEnum.HINT_PASS)], 3, 16)
        abc = decompiler.decompile().nodes[0]
        decompiler = Decompiler25(code, None, 19, 22)
        d = decompiler.decompile()
        hints = [DecompileHint(0, 26, ast.For(abc.nodes[0], abc.expr, d, None), HintEnum.HINT_STATEMENT)]
        self.compileAndCheck(src, None, hints)
        
    def test_def(self):
        src = "def name():\n    pass\n"
        self.compileAndCheck(src)
        src = "def name(arg):\n    pass\n"
        self.compileAndCheck(src)
        src = "def name(arg):\n    name2 = 3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2):\n    pass\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2=0):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1=2, arg2=3):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(*args):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, *args):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2, *args):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2=4, *args):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1=3, arg2=4, *args):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(**kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1=3, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2=4, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1=3, arg2=4, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(*args, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, *args, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2, *args, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1=3, *args, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1, arg2=4, *args, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name(arg1=3, arg2=4, *args, **kwargs):\n    name2 = arg3\n"
        self.compileAndCheck(src)
        src = "def name():\n    (name1 + name2)\n"
        self.compileAndCheck(src)
        src = "def name():\n    def inner():\n        pass\n"
        self.compileAndCheck(src)

    #def testZZZ(self):
        #global test_count
        #print "test count:", test_count

test_count = 0
if __name__ == "__main__":
    unittest.main()
