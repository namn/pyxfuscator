from compiler import ast, transformer, visitor
import random
import sys
import pyxfuscator

pattern = [
    ('I', 'i', 'l', '1'),
    ('O', 'o', '0'),
    ]

generated_names = set()
generated_names.add("")

mapping = {}

def generate_name(max_length=16):
    global pattern
    length = int(random.uniform(max_length / 2, max_length + 1))
    p = random.choice(pattern)
    ret = ""
    for i in range(length):
        if i == 0:
            c = random.choice(p[:-1])
        else:
            c = random.choice(p)
        ret = ret + c
    return ret

def map_name(old_name):
    assert(old_name != "")
    global generated_names, mapping
    if old_name in mapping:
        return mapping[old_name]
    new_name = ""
    while new_name in generated_names:
        new_name = generate_name()
    generated_names.add(new_name)
    mapping[old_name] = new_name
    return new_name
    
    
class Obfuscator(pyxfuscator.PrettyPrinter):
    
    DECRYPT_STRING_FUNCTION = "__Obfuscator_decrypt_string"
    
    def __init__(self, out=sys.stdout, indent=4):
        super(Obfuscator, self).__init__(out, indent)
        self._stop = False
    
    def visitName(self, node):
        assert(isinstance(node, ast.Name))
        node.name = map_name(node.name)
        super(Obfuscator, self).visitName(node)
    
    def visitAssName(self, node):
        assert(isinstance(node, ast.AssName))
        node.name = map_name(node.name)
        super(Obfuscator, self).visitAssName(node)
    
    def visitFunction(self, node):
        assert(isinstance(node, ast.Function))
        node.name = map_name(node.name)
        super(Obfuscator, self).visitFunction(node)
    
    def visitClass(self, node):
        assert(isinstance(node, ast.Class))
        node.name = map_name(node.name)
        super(Obfuscator, self).visitClass(node)
    
    def visitGetattr(self, node):
        node.attrname = map_name(node.attrname)
        super(Obfuscator, self).visitGetattr(node)

    def visitConst(self, node):
        if isinstance(node.value, (str, unicode)) and node.value and not self._stop:
            node.node = ast.Name(Obfuscator.DECRYPT_STRING_FUNCTION)
            node.args = [ast.Const(node.value)]
            del node.value
            node.star_args = None
            node.dstar_args = None
            node.__class__ = ast.CallFunc
            # node = ast.CallFunc(ast.Const('decrypt_str'), node.value)
            self._stop = True
            self.visit(node)
            self._stop = False
            return
        # print node.value
        super(Obfuscator, self).visitConst(node)
    
def main():
    decrypt_str = transformer.parse("""
def %s(s, func=chr):
    r = []
    for c in s:
        c = ord(c) ^ 0x7F
        r.append(func(c))
    return ''.join(r)
""" % Obfuscator.DECRYPT_STRING_FUNCTION)
    tree = transformer.parse("""
class c:
    pass
import b
a['abc'] = u'def'
a[0:] = b
a[:1] = b
a[0:1] = b
a[::] = b
a[::1] = b
a[0:0:1] = b
a[:1:-1] = b
a()
c.a()
b.a()
""")
    tree.node.nodes.insert(0, decrypt_str.node.nodes[0])
    walker = visitor.ExampleASTVisitor()
    #walker = visitor.ASTVisitor()
    walker.VERBOSE = 1
    v = Obfuscator()
    walker.preorder(tree, v)

if __name__ == "__main__":
    main()
